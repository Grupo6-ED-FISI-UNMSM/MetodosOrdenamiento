#include <iostream>
#include <stdlib.h>
#include <math.h>
using namespace std;

struct nodo
{
	int dato;

	nodo *sgte;
	nodo *ante;

};

typedef struct nodo *Tlista;

void agregar(Tlista &lista, int num)
{
	Tlista p = lista;
	Tlista q = new(struct nodo);

	if (lista == NULL)
	{

		q->dato = num;

		q->sgte = lista;
		q->ante = NULL;
		lista = q;
	}
	else
	{

		q->dato = num;

		q->sgte = NULL;
		while (p->sgte != NULL)
		{
			p = p->sgte;

		}
		p->sgte = q;
		q->ante = p;

	}


}
void eliminarPosicion(Tlista &lista, int pos)
{
	if (lista == NULL)
	{
		cout << "No se puede eliminar elementos debido a que la lista esta vacia" << endl;
	}
	else
	{
		if (pos == 1)
		{
			if (lista->sgte == NULL)
			{
				lista = NULL;
			}
			else
			{
				lista = lista->sgte;
				lista->ante = NULL;
			}
		}
		else
		{
			Tlista p, q;
			int contador = 0;
			p = lista;
			while (contador != pos - 1 && p != NULL)
			{
				p = p->sgte;
				contador = contador + 1;
			}
			if (p == NULL)
			{
				cout << "numero fuera de rango" << endl;
			}
			else
			{
				if (p->sgte != NULL)
				{
					q = p;
					p->ante->sgte = p->sgte;
					p->sgte->ante = p->ante;
					delete(q);

				}
				else
				{
					p->ante->sgte = NULL;
					delete(p);
				}
			}
		}
	}
}


void mostrarlista(Tlista &lista)
{
	Tlista p = lista;
	if (p == NULL)
	{
		cout << "Lista vacia" << endl;
	}
	else
	{

		cout << "lista: ";
		while (p != NULL)
		{
			cout << p->dato << " ";
			p = p->sgte;
		}
		cout << "" << endl;
	}
}

void burbuja(Tlista &lista)
{
	Tlista p = lista;
	bool cambio = false;
	int n;

	do
	{
		p = lista;
		cambio = false;
		while (p->sgte != NULL)
		{
			if (p->dato>p->sgte->dato)
			{
				n = p->dato;
				p->dato = p->sgte->dato;
				p->sgte->dato = n;
				cambio = true;
			}
			p = p->sgte;
		}
	} while (cambio == true);
}
void seleccion(Tlista &lista)
{
	bool cambio = false;
	int n;
	Tlista q = lista, p, aux = new(struct nodo);
	aux = NULL;
	do
	{
		cambio = false;
		n = q->dato;
		p = q->sgte;

		do
		{
			if (n > p->dato)
			{
				cambio = true;
				aux = p;
				n = aux->dato;
			}
			p = p->sgte;
		} while (p != NULL);
		if (cambio)
		{
			aux->dato = q->dato;
			q->dato = n;
		}
		q = q->sgte;
	} while (q->sgte != NULL);


}

void insercion(Tlista &lista, int n)
{
	Tlista p = lista, q = new(struct nodo);

	q->dato = n;
	q->sgte = NULL;
	q->ante = NULL;
	if (q->dato <= p->dato)
	{
		q->sgte = lista;
		lista = q;
	}
	else
	{
		while (p->sgte != NULL)
		{
			p = p->sgte;

		}
		if (p->dato <= q->dato)
		{
			p->sgte = q;

		}
		else
		{
			p = lista;
			do
			{
				p = p->sgte;
			} while (q->dato >= p->dato);

			q->sgte = p->ante->sgte;
			q->ante = p->ante;
			p->ante->sgte = q;
			p->ante->sgte->ante = q;


		}
	}


}
void intercambio(Tlista &lista)
{
	Tlista p, q = lista;
	int n, m;
	do
	{
		p = q;
		n = p->dato;
		while (p != NULL)
		{
			if (n>p->dato)
			{
				m = n;
				n = p->dato;
				p->dato = m;
			}

			p = p->sgte;

		}
		q->dato = n;
		q = q->sgte;
	} while (q->sgte != NULL);
}
void shellSort(Tlista &lista)
{
	int cont = 0;
	Tlista p = lista, aux = lista;
	while (p != NULL)
	{
		cont++;
		p = p->sgte;
	}
	p = lista;
	int salto = cont;
	bool cambio = false;
	do
	{

		salto = salto / 2;
		do
		{

			if (aux->sgte == NULL)
			{
				p = lista;
				cambio = false;
			}


			aux = p;

			for (int i = 0; i<salto; i++)
				aux = p->sgte;

			if (p->dato>aux->dato)
			{
				int numAux = p->dato;
				p->dato = aux->dato;
				aux->dato = numAux;
				cambio = true;
			}
			p = p->sgte;
		} while (cambio == true || aux->sgte != NULL);
		p = lista;
	} while (salto != 1);

}
void quickSort(Tlista pivote, Tlista pIzq, Tlista pDer, Tlista &lista)
{

	Tlista iz = new(struct nodo), der = new(struct nodo);
	iz = pIzq;
	der = pDer;
	do
	{
		while (iz->dato<pivote->dato)
			iz = iz->sgte;

		while (der->dato>pivote->dato)
			der = der->ante;

		if (iz != pivote || der != pivote)
		{
			if (iz == pivote)
			{
				int aux;
				aux = der->dato;
				der->dato = iz->dato;
				iz->dato = aux;

				pivote = der;
				iz = iz->sgte;
			}
			else
			{
				if (der == pivote)
				{
					int aux;
					aux = iz->dato;
					iz->dato = der->dato;
					der->dato = aux;

					pivote = iz;
					der = der->ante;

				}
				else
				{
					int aux;
					aux = iz->dato;
					iz->dato = der->dato;
					der->dato = aux;

					iz = iz->sgte;
					der = der->ante;

				}
			}

		}


	} while (iz != pivote || der != pivote);

	if (pIzq != pivote)
		quickSort(pivote->ante, pIzq, pivote->ante, lista);
	if (pDer != pivote)
		quickSort(pivote->sgte, pivote->sgte, pDer, lista);

}
void ordenarRadix(Tlista &list)
{
	Tlista posAux = list, p = list;
	int max = posAux->dato;
	int cifrasMax = 0, cont = 0;
	while (p != NULL)
	{
		if (max < p->dato)
			max = p->dato;
		p = p->sgte;
	}

	do{
		cifrasMax++;
		max = (int)(max / 10);
	} while (max>0);


	do{
		p = list;
		posAux = list;
		cont++;
		//Recorrer� las diez posiciones de 0 a 9 y las ordenar�
		for (int i = 0; i<10; i++)
		{
			p = posAux;
			while (p != NULL)
			{
				int auxCont = 0;
				int auxNum = p->dato;
				//Hallar la cantidad de d�gitos del n�mero
				do{
					auxCont++;
					auxNum = (int)(auxNum / 10);
				} while (max>0);
				//Arroja la cifra que corresponda al recorrido. Si contador es 1 es el �ltimo d�gito, si es 2 es el pen�ltimo...
				if (auxCont<cont + 1)
					auxNum = (int)(p->dato / pow(10, cont - 1));
				else
				{
					auxNum = (int)(p->dato / pow(10, cont - 1));
					auxNum = auxNum % 10;
				}
				//Cambiara la posici�n de los valores si concuerda con el d�gito en orden
				if (auxNum == i)
				{
					auxNum = posAux->dato;
					posAux->dato = p->dato;
					p->dato = auxNum;
					posAux = posAux->sgte;
				}
				p = p->sgte;

			}

		}
	} while (cont<cifrasMax + 1);
}
Tlista anadir_nodo_fin_lista(Tlista &nodo1, Tlista &lista) {
	Tlista aux = NULL, ant = NULL, nodo_aux = new(struct nodo);
	nodo_aux->dato = nodo1->dato;
	nodo_aux->sgte = NULL;
	ant = lista;
	if (ant != NULL) {
		aux = ant->sgte;
		while (aux != NULL) {
			ant = aux;
			aux = aux->sgte;
		}
		ant->sgte = nodo_aux;
	}
	else {
		lista = nodo_aux;
	}
	return lista;
}
Tlista mezclar(Tlista &lista1, Tlista &lista2){
	Tlista listamezclada = NULL, aux1 = lista1, aux2 = lista2;
	while (aux1 != NULL && aux2 != NULL){
		if (aux1->dato<aux2->dato){
			listamezclada = anadir_nodo_fin_lista(aux1, listamezclada);
			aux1 = aux1->sgte;
		}
		else{
			listamezclada = anadir_nodo_fin_lista(aux2, listamezclada);
			aux2 = aux2->sgte;
		}
	}

	while (aux1 != NULL){
		listamezclada = anadir_nodo_fin_lista(aux1, listamezclada);
		aux1 = aux1->sgte;
	}
	while (aux2 != NULL) {
		listamezclada = anadir_nodo_fin_lista(aux2, listamezclada);
		aux2 = aux2->sgte;
	}

	return listamezclada;
}
int longitud_lista(Tlista &lista) {
	int longitud = 0;
	Tlista aux = NULL;

	aux = lista;

	while (aux != NULL) {
		longitud++;
		aux = aux->sgte;
	}

	return longitud;
}
Tlista obtener_lista2(Tlista &lista) {
	Tlista lista_aux = new(struct nodo), aux = NULL;
	int longitud = 0, mitad = 0;


	longitud = longitud_lista(lista);
	if (longitud % 2)
		mitad = (longitud / 2) + 1;
	else
		mitad = longitud / 2;

	aux = lista;
	while (mitad - 1 > 0) {
		aux = aux->sgte;
		mitad--;
	}
	lista_aux = aux->sgte;

	aux->sgte = NULL;

	return lista_aux;
}

Tlista mergeSort(Tlista &lista) {
	Tlista lista1 = NULL, lista2 = NULL, listaOrd1 = NULL, listaOrd2 = NULL, listaOrd = NULL;


	if (longitud_lista(lista) <= 1) {
		return lista;
	}
	else {

		lista1 = lista;

		lista2 = obtener_lista2(lista);

		listaOrd1 = mergeSort(lista1);
		listaOrd2 = mergeSort(lista2);


		listaOrd = mezclar(listaOrd1, listaOrd2);

		return listaOrd;
	}
}

void menu()
{
	cout << endl << endl;
	cout << "\t\t" << char(178) << char(178) << char(178) << char(178) << char(178) << char(178) << char(178) << char(178) << char(178) << char(178) << char(178) << char(178) << char(178) << char(178) << char(178) << char(178) << char(178) << char(178) << "MENU" << char(178) << char(178) << char(178) << char(178) << char(178) << char(178) << char(178) << char(178) << char(178) << char(178) << char(178) << char(178) << char(178) << char(178) << char(178) << char(178) << char(178) << char(178) << char(178) << char(178) << char(178) << char(178) << char(178) << endl;
	cout << "\t\t" << char(178) << char(178) << "1)Agregar a lista" << "\t\t\t   " << char(178) << char(178) << endl;
	cout << "\t\t" << char(178) << char(178) << "2)Eliminar de la lista                   " << char(178) << char(178) << endl;
	cout << "\t\t" << char(178) << char(178) << "3)Mostrar lista                          " << char(178) << char(178) << endl;
	cout << "\t\t" << char(178) << char(178) << "4)Mostrar lista ordenada por burbuja     " << char(178) << char(178) << endl;
	cout << "\t\t" << char(178) << char(178) << "5)Mostrar lista ordenada por seleccion   " << char(178) << char(178) << endl;
	cout << "\t\t" << char(178) << char(178) << "6)Agregar por insercion                  " << char(178) << char(178) << endl;
	cout << "\t\t" << char(178) << char(178) << "7)Mostrar lista ordenada por intercambio " << char(178) << char(178) << endl;
	cout << "\t\t" << char(178) << char(178) << "8)Mostrar lista ordenada por shellsort   " << char(178) << char(178) << endl;
	cout << "\t\t" << char(178) << char(178) << "9)Mostrar lista ordenada por quicksort   " << char(178) << char(178) << endl;
	cout << "\t\t" << char(178) << char(178) << "10)Mostrar lista ordenada por mergesort  " << char(178) << char(178) << endl;
	cout << "\t\t" << char(178) << char(178) << "11)Mostrar lista ordenada por radix      " << char(178) << char(178) << endl;
	cout << "\t\t" << char(178) << char(178) << "12)Salir                                 " << char(178) << char(178) << endl;
	cout << "\t\t" << char(178) << char(178) << char(178) << char(178) << char(178) << char(178) << char(178) << char(178) << char(178) << char(178) << char(178) << char(178) << char(178) << char(178) << char(178) << char(178) << char(178) << char(178) << char(178) << char(178) << char(178) << char(178) << char(178) << char(178) << char(178) << char(178) << char(178) << char(178) << char(178) << char(178) << char(178) << char(178) << char(178) << char(178) << char(178) << char(178) << char(178) << char(178) << char(178) << char(178) << char(178) << char(178) << char(178) << char(178) << char(178) << endl;

	cout << "\t\t" << endl << "Elija su opcion: ";

}
int main()
{
	Tlista lista = new (struct nodo), listaordenada = new (struct nodo);
	lista = NULL; listaordenada = NULL;
	Tlista pIzq = new (struct nodo), pDer = new (struct nodo), pivote = new (struct nodo);
	pIzq = NULL;
	pDer = NULL;
	pivote = NULL;
	int numQ, cont = 1;
	int opc, num, pos;
	do
	{
		menu();
		cin >> opc;
		switch (opc)
		{
		case 1:

			cout << "digite el dato que desea agregar: ";
			cin >> num;
			agregar(lista, num);
			break;
		case 2:
			cout << "digite la posicion que desea eliminar: ";
			cin >> pos;
			eliminarPosicion(lista, pos);
			break;
		case 3:
			mostrarlista(lista);
			break;
		case 4:
			burbuja(lista);
			mostrarlista(lista);
			break;
		case 5:
			seleccion(lista);
			mostrarlista(lista);
			break;
		case 6:
			cout << "digite el dato que desea agregar: ";
			cin >> num;
			insercion(lista, num);
			break;
		case 7:
			intercambio(lista);
			mostrarlista(lista);
			break;
		case 8:
			shellSort(lista);
			mostrarlista(lista);
			break;
		case 9:
			if (lista == NULL)
			{
				cout << "la lista esta vacia por lo tanto no se puede ordenar" << endl;
			}
			else
			{
				cout << "\n\tEstablezca el pivote, ingrese la posicion: ";
				cin >> num;
				pIzq = lista;
				pDer = lista;
				while (pDer->sgte != NULL)
				{
					if (cont == num)
						pivote = pDer;

					cont++;
					pDer = pDer->sgte;
					if (pDer->sgte == NULL)
					if (cont == num)
						pivote = pDer;

				}
				cout << "\n\tPivote: " << pivote->dato;
				cout << "\n\tPIzquierdo: " << pIzq->dato;
				cout << "\n\tPDerecho: " << pDer->dato << endl;

				quickSort(pivote, pIzq, pDer, lista);
				mostrarlista(lista);
			}
			break;
		case 10:
			listaordenada = mergeSort(lista);
			mostrarlista(listaordenada);
			break;
		case 11:
			ordenarRadix(lista);
			mostrarlista(lista);
			break;
		case 12:
			return 0;

		default:
			cout << "numero fuera de rango" << endl;


		}
		system("pause");
		system("cls");
	} while (opc || opc == 0);

}